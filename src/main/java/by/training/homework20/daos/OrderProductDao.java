package by.training.homework20.daos;

import by.training.homework20.entities.OrderProduct;
import by.training.homework20.entities.Product;

import java.util.List;

public interface OrderProductDao {
    void add(OrderProduct orderProduct);

    List<Product> getChosenList(long orderId);

}
