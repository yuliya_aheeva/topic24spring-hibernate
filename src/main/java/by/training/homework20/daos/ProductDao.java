package by.training.homework20.daos;

import by.training.homework20.entities.Product;

import java.util.List;

public interface ProductDao {
    List<Product> getAllProducts();
}
