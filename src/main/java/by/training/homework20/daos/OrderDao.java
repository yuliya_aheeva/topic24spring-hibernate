package by.training.homework20.daos;

import by.training.homework20.entities.Order;

import java.util.Optional;

public interface OrderDao {

    Long add(Order order);

    Optional<Order> getById(long orderId);

    void updateTotalPrice(long orderId, double totalPrice);

}
