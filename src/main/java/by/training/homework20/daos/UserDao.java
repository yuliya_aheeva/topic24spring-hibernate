package by.training.homework20.daos;

import by.training.homework20.entities.User;

import java.util.Optional;

public interface UserDao {

    Long add(User user);

    Optional<User> findUserByName(String name);

    Optional<User> findUserById(long userId);

}
