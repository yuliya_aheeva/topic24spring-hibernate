package by.training.homework20.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "USERS")
public class User implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;
    @Column(name = "USER_NAME", nullable = false)
    private String name;
    @Column(name = "PASSWORD")
    private String password;
    private UserRole userRole;
    
    public User(String username, String password) {
        this.name = username;
        this.password = password;
    }
}
