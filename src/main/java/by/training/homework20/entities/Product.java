package by.training.homework20.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PRODUCTS")
public class Product implements Serializable {

    private static final long serialVersionUID = 44L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;
    @Column(name = "TITLE", nullable = false)
    private String title;
    @Column(name = "PRICE", nullable = false)
    private Double price;

    public Product(String title, Double price) {
        this.title = title;
        this.price = price;
    }

    public Product(String stringIdNamePrice) {
        this.id = Long.valueOf(stringIdNamePrice.substring(0, stringIdNamePrice.indexOf(".")));
        this.title = stringIdNamePrice.substring(stringIdNamePrice.indexOf(" "), stringIdNamePrice.indexOf(","));
        this.price = Double.valueOf(stringIdNamePrice.substring(stringIdNamePrice.indexOf("$") + 1));
    }

    public String getTitlePrice() {
        return id + ". " + title + ", $" + price;
    }

    @Override
    public String toString() {
        return id +
                ". " + title +
                ", $" + price;
    }
}
