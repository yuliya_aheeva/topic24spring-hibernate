package by.training.homework20.entities;

public enum UserRole {
    USER, ADMIN
}
