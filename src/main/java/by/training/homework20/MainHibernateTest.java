package by.training.homework20;

import by.training.homework20.entities.Order;
import by.training.homework20.entities.OrderProduct;
import by.training.homework20.entities.Product;
import by.training.homework20.entities.User;
import by.training.homework20.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import java.util.List;

@Component
public class MainHibernateTest {
    private SessionFactory sessionFactory;

    public MainHibernateTest(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void run() {
//users
        User person1 = new User("Alex", "alex");
        User person2 = new User("Victor", "22L");
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();


            session.save(person1);
            session.save(person2);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM User U where name = :nameParam");
            query.setParameter("nameParam", "Alex");
            User user = (User) query.getSingleResult();
            System.out.println(user);

    } catch (Throwable cause) {
        cause.printStackTrace();
    }

        List<User> list = null;

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM User");
            list = (List<User>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (list != null && !list.isEmpty()) {
            for (User person : list) {
                System.out.println(person);
            }
        }
//items

        List<Product> goods = null;

         try (Session session = sessionFactory.openSession()) {
        session.beginTransaction();


        Query query = session.createQuery("FROM Product");
            goods = (List<Product>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (list != null && !list.isEmpty()) {
            for (Product item : goods) {
                System.out.println(item);
            }
        }
// orders
        Order order1 = new Order(person1, 5.36);
        Order order2 = new Order(person1, 8.34);
        Order order3 = new Order(person2, 0.0);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();


            session.save(order1);


            session.save(order2);


            session.save(order3);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        List<Order> orders = null;

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM Order");
            orders = (List<Order>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (orders != null && !orders.isEmpty()) {
            for (Order item : orders) {
                System.out.println(item);
            }
        }
// orders_goods
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            OrderProduct orderG1 = new OrderProduct(order1, goods.get(0));
            session.save(orderG1);

            OrderProduct orderG2 = new OrderProduct(order1, goods.get(1));
            session.save(orderG2);

            OrderProduct orderG3 = new OrderProduct(order3, goods.get(0));
            session.save(orderG3);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        List<OrderProduct> ordersG = null;

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM OrderProduct");
            ordersG = (List<OrderProduct>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (ordersG != null && !ordersG.isEmpty()) {
            for (OrderProduct orderGood : ordersG) {
                System.out.println(orderGood);
            }
        }

        List<Product> productsFrom = null;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("select productId from OrderProduct op where op.orderId.id = :orderParam");
            query.setParameter("orderParam", 3L);


            productsFrom = (List<Product>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (ordersG != null && !ordersG.isEmpty()) {
            for (OrderProduct orderGood : ordersG) {
                System.out.println(orderGood);
            }
        }

//updates
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("update Order set totalPrice = :total where id = :idParam");
            query.setParameter("idParam", 1L);
            query.setParameter("total", 6.16);

            query.executeUpdate();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM OrderProduct");
            ordersG = (List<OrderProduct>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (ordersG != null && !ordersG.isEmpty()) {
            for (OrderProduct orderGood : ordersG) {
                System.out.println(orderGood);
            }
        }
    }
}
