package by.training.homework20.controllers;

import by.training.homework20.MainHibernateTest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/test")
public class TestController {
    private final MainHibernateTest test;

    public TestController(MainHibernateTest test) {
        this.test = test;
    }

    @GetMapping
    public String get() {
        test.run();
        return "hello";
    }
}
