package by.training.homework20.controllers;

import by.training.homework20.services.security.CustomUserDetails;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
public class UserController {

    @GetMapping("/register")
    public String registerNewUser() {
        return "registration";
    }

    @GetMapping("/welcome")
    public String doGetCatalogPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, HttpServletRequest request) {
        final CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(principal.getUser());
        request.getSession().setAttribute("userName", customUserDetails.getUser().getName());
        request.getSession().setAttribute("userId", customUserDetails.getUser().getId());
        return "redirect:preorder";
    }
}
