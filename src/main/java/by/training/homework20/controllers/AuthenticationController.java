package by.training.homework20.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/authentication")
public class AuthenticationController {

    @PostMapping()
    public String doPostAuthPage(HttpServletRequest request) {
        if (request.getParameter("button") != null) {
            return "redirect:login";
        } else {
            return "redirect:register";
        }
    }
}
