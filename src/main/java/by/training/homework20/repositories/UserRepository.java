package by.training.homework20.repositories;

import by.training.homework20.daos.UserDao;
import by.training.homework20.entities.User;
import by.training.homework20.entities.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Optional;

@Repository
public class UserRepository implements UserDao {
    private static final Logger LOGGER = LogManager.getLogger(UserRepository.class);

    private final SessionFactory sessionFactory;

    public UserRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long add(User user) {
        final Long savedPersonId = (Long) sessionFactory.getCurrentSession().save(user);
        LOGGER.info("Saved person id: {}", savedPersonId);
        return savedPersonId;
    }

    @Override
    public Optional<User> findUserByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM User U where name = :nameParam");
        query.setParameter("nameParam", name);
        User user = (User) query.getSingleResult();
        if (user == null) {
            LOGGER.info("user doesn't exist");
            return Optional.empty();
        } else {
            user.setUserRole(UserRole.USER);
            LOGGER.info("user exists" + user);
            return Optional.of(user);
        }
    }

    @Override
    public Optional<User> findUserById(long userId) {
        final User user = sessionFactory.getCurrentSession().find(User.class, userId);
        if (user == null) {
            LOGGER.info("user doesn't exist");
            return Optional.empty();
        } else {
            LOGGER.info("User with id exist{}", user);
            return Optional.of(user);
        }
    }
}
