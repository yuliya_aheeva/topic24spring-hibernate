package by.training.homework20.repositories;

import by.training.homework20.daos.ProductDao;
import by.training.homework20.entities.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ProductRepository implements ProductDao {
    private static final Logger LOGGER = LogManager.getLogger(UserRepository.class);

    private final SessionFactory sessionFactory;

    public ProductRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Product> getAllProducts() {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Product");
        List<Product> products = (List<Product>) query.getResultList();
        LOGGER.info("products exist" + products);
        return products;
    }
}
