package by.training.homework20.repositories;

import by.training.homework20.daos.OrderDao;
import by.training.homework20.entities.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class OrderRepository implements OrderDao {
    private static final Logger LOGGER = LogManager.getLogger(UserRepository.class);

    private final SessionFactory sessionFactory;

    public OrderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long add(Order order) {
        final Long savedOrderId = (Long) sessionFactory.getCurrentSession().save(order);
        LOGGER.info("Saved order id: {}", savedOrderId);
        return savedOrderId;
    }


    @Override
    public Optional<Order> getById(long orderId) {
        final Order order = sessionFactory.getCurrentSession().find(Order.class, orderId);
        if (order == null) {
            LOGGER.info("user doesn't exist");
            return Optional.empty();
        } else {
            LOGGER.info("User with id exist{}", order);
            return Optional.of(order);
        }
    }

    @Override
    public void updateTotalPrice(long orderId, double totalPrice) {
        final Order order = sessionFactory.getCurrentSession().get(Order.class, orderId);

        LOGGER.info("Order for update: {}", order);

        order.setTotalPrice(totalPrice);

        sessionFactory.getCurrentSession().saveOrUpdate(order);

        LOGGER.info("Order after update: {}", order);

    }
}
