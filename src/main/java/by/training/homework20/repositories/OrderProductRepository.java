package by.training.homework20.repositories;

import by.training.homework20.daos.OrderProductDao;
import by.training.homework20.entities.OrderProduct;
import by.training.homework20.entities.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class OrderProductRepository implements OrderProductDao {
    private static final Logger LOGGER = LogManager.getLogger(UserRepository.class);

    private final SessionFactory sessionFactory;

    public OrderProductRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(OrderProduct orderProduct) {
        final Long savedId = (Long) sessionFactory.getCurrentSession().save(orderProduct);
        LOGGER.info("Saved OrderProduct id: {}", savedId);
    }

    @Override
    public List<Product> getChosenList(long orderId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select productId from OrderProduct op where op.orderId.id = :orderIdParam");
        query.setParameter("orderIdParam", orderId);
        List<Product> products = (List<Product>) query.getResultList();
        LOGGER.info("chosen products" + products);
        return products;
    }
}
