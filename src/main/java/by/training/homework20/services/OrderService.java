package by.training.homework20.services;

import by.training.homework20.daos.OrderDao;
import by.training.homework20.entities.Order;
import by.training.homework20.entities.User;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Getter
@Transactional
public class OrderService {
    private final OrderDao orderDao;

    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public Long createNewOrder(final User user) {
        Order order = new Order(user, 0.);
        return orderDao.add(order);
    }

    public Optional<Order> getOrderById(final long orderId) {
        return orderDao.getById(orderId);
    }

    public void updateTotalPrice(long orderId, double totalPrice) {
        orderDao.updateTotalPrice(orderId, totalPrice);
    }
}
