package by.training.homework20.services;

import by.training.homework20.daos.OrderProductDao;
import by.training.homework20.entities.Order;
import by.training.homework20.entities.OrderProduct;
import by.training.homework20.entities.Product;
import by.training.homework20.entities.User;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Getter
@Transactional
public class OrderProductService {
    private final OrderProductDao orderProductDao;

    public OrderProductService(OrderProductDao orderProductDao) {
        this.orderProductDao = orderProductDao;
    }

    public void addOrderProduct(final Order order, final Product product) {
        OrderProduct orderProduct = new OrderProduct(order, product);
        orderProductDao.add(orderProduct);
    }

    public List<Product> getChosenProducts(long orderId) {
        return orderProductDao.getChosenList(orderId);
    }
}
