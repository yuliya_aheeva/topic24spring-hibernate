package by.training.homework20.services;

import by.training.homework20.entities.Order;
import by.training.homework20.entities.Product;
import by.training.homework20.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ShopService {
    private final UserService userService;
    private final OrderService orderService;
    private final ProductService productService;
    private final OrderProductService orderProductService;
    private final PasswordEncoder encoder;

    public void registerUser(HttpServletRequest request) {
        userService.registerUser(
                request.getParameter("userName"),
                encoder.encode(request.getParameter("userPassword"))
        );
    }

    public HttpSession setAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession();
        createOrder(session);
        setProductListAttribute(session);
        setChosenProductsAttribute(session);
        return session;
    }

    private void createOrder(HttpSession session) {
        Optional<User> user = userService.findById((long) session.getAttribute("userId"));
        if (user.isPresent()) {
            Long orderId = orderService.createNewOrder(user.get());
            session.setAttribute("orderId", orderId);
        }
    }

    private void setProductListAttribute(HttpSession session) {
        String chosenProducts = productService.getChosenProducts(
                orderProductService.getChosenProducts((long) session.getAttribute("userId")));
        session.setAttribute("chosenProducts", chosenProducts);
        List<Product> productList = productService.getProductsAsList();
        session.setAttribute("productList", productList);
    }

    private void setChosenProductsAttribute(HttpSession session) {
        String chosenProducts = productService.getChosenProducts(
                orderProductService.getChosenProducts((long) session.getAttribute("orderId")));
        session.setAttribute("chosenProducts", chosenProducts);
    }

    public void addProductToList(HttpServletRequest request) {
        Product product = new Product(request.getParameter("productOrder"));
        Optional<Order> order = orderService.getOrderById((long) request.getSession().getAttribute("orderId"));
        order.ifPresent(value -> orderProductService.addOrderProduct(value, product));
        setChosenProductsAttribute(request.getSession());
    }

    public void submitOrder(HttpServletRequest request) {
        long orderId = (long) request.getSession().getAttribute("orderId");
        double total = 0;
        List<Product> chosen = orderProductService.getChosenProducts(orderId);
        for(Product product : chosen) {
            total += product.getPrice();
        }
        orderService.updateTotalPrice(orderId, total);
        request.getSession().setAttribute("totalPrice", total);
    }
}
