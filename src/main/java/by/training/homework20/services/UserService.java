package by.training.homework20.services;

import by.training.homework20.daos.UserDao;
import by.training.homework20.entities.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserService {
    private final UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public void registerUser(final String userName, final String userPassword) {
        User user = new User();
        user.setName(userName);
        user.setPassword(userPassword);
        userDao.add(user);
    }

    public Optional<User> findById(long userId) {
        return userDao.findUserById(userId);
    }
}
