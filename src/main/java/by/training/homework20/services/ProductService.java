package by.training.homework20.services;

import by.training.homework20.entities.Product;
import by.training.homework20.daos.ProductDao;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Getter
@Transactional
public class ProductService {
    private final ProductDao productDao;

    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public List<Product> getProductsAsList() {
        List<Product> products = productDao.getAllProducts();
        return products;
    }

    public String getChosenProducts(List<Product> products) {
        StringBuilder chosenProducts = new StringBuilder();
        Set<String> setProducts = new HashSet<>();
        for (Product product : products) {
            chosenProducts.append(product.toString());
            setProducts.add(product.getTitle());
        }
        StringBuilder productsInCart = new StringBuilder();
        for (String product : setProducts) {
            Pattern pattern = Pattern.compile(product);
            Matcher matcher = pattern.matcher(chosenProducts.toString());
            int count = 0;
            while (matcher.find()) {
                count++;
            }
            productsInCart.append("" + product + " " +  count + "pcs. ");
        }
        return productsInCart.toString();
    }
}
