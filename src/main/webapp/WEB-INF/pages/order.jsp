<html>
<head>
    <title>Title</title>
</head>
<body>
    <div style="margin: 40px 40px 40px 100px" th:text="'Dear ' + ${session.userName} + ', your order:'"></div>
    <div style="margin: 40px 40px 40px 100px" th:text="${session.chosenProducts}"></div>
    <div style="margin: 40px 40px 40px 100px" th:text="'Total price: $' + ${session.totalPrice} "></div>
</body>
</html>
