<jsp:useBean id="product" scope="session" class="by.training.homework20.entities.Product"/>

<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8" />
    <title>Make your order</title>
</head>
<body>
<div style="margin: 40px 40px 40px 100px" th:text="'Hello ' + ${session.userName} + '!'">
</div>

<div style="margin: 40px 40px 40px 100px;">
    <p>Make your order</p>
    <p>You have already chosen:</p>
    <form method="POST">
        <ul th:text="${session.chosenProducts}"></ul>
        <label>
            <select name="productOrder" >
                <option th:each="product : ${session.productList}"
                        th:value="${product.getTitlePrice()}"
                        th:utext="${product.getTitlePrice()}"></option>
            </select>
        </label><br><br>
        <input type="submit" name="addItem" value="Add item">  <input type="submit" name="submit" value="Submit">
    </form>
</div>
</body>
</html>